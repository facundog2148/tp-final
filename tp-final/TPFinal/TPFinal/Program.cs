﻿using System;
using System.Collections.Generic;

namespace TPFinal
{
    class Program
    {
        static void Main(string[] args)
        {
            string Nombre;
            double Ganancias = 0;
            Console.WriteLine("Escribir nombre de la empresa:");
            Nombre = Console.ReadLine();
            Empresa empresa = new Empresa(Nombre);
            List<Empleado> Empleados = new List<Empleado>();
            Area Area1 = new Area("Marketing");
            Area Area2 = new Area("Contabilidad");
            Area Area3 = new Area("Dirección");

            bool salir = false;
            while (!salir)
            {
                Console.Clear();
                int ejercicios;

                Console.WriteLine("1- Contratar empleados. \n" +
                "2- Mostrar empleados por área. \n" +
                "3- Mostrar a todos los empleados. \n" +
                "4- Aumentar el salario a un empleado en específico. \n" +
                "5- Aumentar el salario a todos los empleados de un área. \n" +
                "6- Aumentar el salario a todos los empleados. \n" +
                "7- Aumentar el salario a los empleados que tengan más de X años de servicio (La cantidad de años debe poder ser introducida por consola). \n" +
                "8- Despedir un empleado. \n" +
                "9- Aumentar stock de un determinado producto(El producto debe ser elegido dentro de los productos que vende la empresa). \n" +
                "10- Vender un producto: Debe mostrar los diferentes productos que vende la empresa con sus respectivos stocks.No se debe poder vender más productos de los que hay en stock. \n" +
                "11- Calcular ganancias de la empresa.Debe devolver la suma de los valores de todos los productos vendidos hasta el momento.Al iniciar el programa, como aún no se ha vendido nada, debe devolver 0. \n" +
                "12- Salir");

                ejercicios = int.Parse(Console.ReadLine());

                switch (ejercicios)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("¿Cuántas áreas de empleados desea contratar? (1, 2 o 3)");
                        double Cantidad_Areas = Convert.ToDouble(Console.ReadLine());
                        while(Cantidad_Areas < 4)
                        {
                            if (Cantidad_Areas == 1)
                            {
                                Programadores Adm1 = new Programadores("Patricia", "Hernandez", 31, "F", 35000, 10, "Programador", "Senior", "Java", 8);
                                Empleados.Add(Adm1);
                                Area1.agregarEmpleado(Adm1);
                                break;
                            }
                            else if (Cantidad_Areas == 2)
                            {
                                Programadores Prog1 = new Programadores("Patricia", "Hernandez", 31, "F", 35000, 10, "Programador", "Senior", "Java", 8);
                                Empleados.Add(Prog1);
                                Area1.agregarEmpleado(Prog1);
                                break;
                            }
                            else
                            {
                                Diseñadores Dis1 = new Diseñadores("Fernando", "Sosa", 29, "M", 30000, 7, "Diseñador", "C", 8);
                                Empleados.Add(Dis1);
                                Area1.agregarEmpleado(Dis1);
                                break;
                            }
                        }
                        break;
                    case 2:
                        int areas;
                        Console.WriteLine("Elige una de las 3 areas (1,2 o 3)");
                        areas = int.Parse(Console.ReadLine());
                        switch (areas)
                        {
                            case 1:
                                Area1.Jefe();
                                Console.WriteLine(Area1);
                                break;
                            case 2:
                                Area2.Jefe();
                                Console.WriteLine(Area2);
                                break;
                            case 3:
                                Area3.Jefe();
                                Console.WriteLine(Area3);
                                break;
                            default:
                                Console.WriteLine("Intente nuevamente");
                                break;
                        }

                        break;
                    case 3:

                        foreach (Empleado i in Empleados)
                        {
                            Console.WriteLine(i);
                        }
                        Console.ReadKey();

                        break;
                    case 4:
                        int contador = 0;
                        int Empleado;
                        double aumento;

                        foreach (Empleado i in Empleados)
                        {
                            Console.WriteLine(contador + " " + i.nombre + " " + i.apellido);
                            contador++;
                            Console.WriteLine(" ");
                        }
                        Console.WriteLine("Elija el numero del empleado para aumentar su sueldo:");
                        Empleado = int.Parse(Console.ReadLine());
                        Console.WriteLine("Ingrese el valor a aumentar en el sueldo de dicho empleado:");
                        aumento = double.Parse(Console.ReadLine());
                        Empleados[Empleado].AumentoSalario(aumento);
                        Console.WriteLine("Sueldo actualizado: " + Empleados[Empleado].sueldo);
                        Console.ReadKey();
                        break;
                    case 5:
                        int nArea;
                        double Aumento;
                        Console.Clear();
                        Console.WriteLine("Ingrese el valor a aumentar en los salarios");
                        Aumento = double.Parse(Console.ReadLine());
                        Console.WriteLine("Elija el area del empleado al que desea aumentarle su sueldo");
                        nArea = int.Parse(Console.ReadLine());


                        Console.ReadKey();
                        break;
                    case 6:

                        Console.ReadKey();
                        break;
                    case 7:
                        int años;
                        int cant;
                        Console.WriteLine("Ingresar cantidad de aumento en años");
                        años = int.Parse(Console.ReadLine());

                        Console.WriteLine("Cantidad a aumentar");
                        cant = int.Parse(Console.ReadLine());

                        foreach (Empleado i in Empleados)
                        {
                            int a = i.años_servicio;
                            int b = a / años;

                            if (b >= 1)
                            {
                                i.AumentoSalario(b * cant);
                            }
                            else
                            {

                            }

                        }

                         break;
                    case 8:

                        Console.ReadKey();
                        break;
                    case 9:
                        int Stock;
                        int Aumento_stock;
                        Console.WriteLine("Seleccionar producto para aumentar stock:");
                        Console.WriteLine("1- Auricular  \n " + "2- Bateria  \n" + "3- Cargador \n");
                    

                    break;
                    case 10:
                        

                        break;
                    case 11:
                        Console.Clear();
                        if (Ganancias > 0)
                        {
                            Console.WriteLine("Las ganancias de la empresa son: {0}", Ganancias);
                        }
                        else
                        {
                            Console.WriteLine("La empresa todavía no genera ganancias");
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case 12:
                        salir = true;
                        Console.ReadKey();
                        break; ;

                }


            }

        }
    }
}


