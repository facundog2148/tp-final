﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    class Cargador : Producto
    {
        private string Tipo_cargador;
        private double Vel_carga;

        public Cargador(string Nombre, int Precio, string Marca, int Cantidad, string Tipo_cargador, double Vel_carga) : base(Nombre, Precio, Marca, Cantidad)
        {
            this.Tipo_cargador = Tipo_cargador;
            this.Vel_carga = Vel_carga;
        }

        public string tipo_cargador
        {
            get
            {
                return Tipo_cargador;
            }
            set
            {
                Tipo_cargador = value;
            }
        }
        public double vel_carga
        {
            get
            {
                return Vel_carga;
            }
            set
            {
                Vel_carga = value;
            }
        }

        public int Stock
        {
            get
            {
                return base.Cantidad;

            }
            set
            {
                this.Cantidad = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}, Tipo de cargador: {1}, Velocidad de carga: {2}", base.toString(), Tipo_cargador, Vel_carga);
        }


    }
}
