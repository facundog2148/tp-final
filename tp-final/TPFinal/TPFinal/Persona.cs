﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    class Persona
    {
        protected string Nombre;
        protected string Apellido;
        protected int Edad;
        protected string Sexo;

        public Persona(string Nombre, string Apellido, int Edad, string Sexo)
        {
            this.Nombre = Nombre;
            this.Apellido = Apellido;
            this.Edad = Edad;
            this.Sexo = Sexo;
        }

        public string nombre
        {
            get
            {
                return Nombre;
            }
            set
            {
                Nombre = value;
            }
        }

        public string apellido
        {
            get
            {
                return Apellido;
            }
            set
            {
                Apellido = value;
            }
        }
        public int edad
        {
            get
            {
                return Edad;
            }
            set
            {
                Edad = value;
            }
        }
        public string sexo
        {
            get
            {
                return Sexo;
            }
            set
            {
                Sexo = value;
            }
        }


        public override string ToString()
        {
            return String.Format("Nombre: {0}, Apellido: {1}, Edad: {2}, Sexo: {3}", Nombre, Apellido, Edad, Sexo);
        }


    }
}
