﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    class Diseñadores : Empleado
    {
        private string Cargo;
        private string Tipo_dis;
        private int Carga_horaria;

        public Diseñadores(string Nombre, string Apellido, int Edad, string Sexo, double Sueldo, int Años_servicio, string Cargo , string Tipo_dis, int Carga_horaria) : base(Nombre, Apellido, Edad, Sexo, Sueldo, Años_servicio)
        {
            this.Cargo = Cargo;
            this.Tipo_dis = Tipo_dis;
            this.Carga_horaria = Carga_horaria;
        }

        public string cargo
        {
            get
            {
                return Cargo;
            }
            set
            {
                Cargo = value;
            }
        }

        public string tipo_dis
        {
            get
            {
                return Tipo_dis;
            }
            set
            {
                Tipo_dis = value;
            }
        }

        public int carga_horaria
        {
            get
            {
                return Carga_horaria;
            }
            set
            {
                Carga_horaria = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}, Cargo: {1}, Tipo de diseñador: {2}, Carga horaria {3}", base.ToString(), Cargo, Tipo_dis,Carga_horaria);
        }
    }
}
