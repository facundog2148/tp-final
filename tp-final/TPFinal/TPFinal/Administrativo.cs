﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    class Administrativo : Empleado
    {
        private string Cargo;
        private int Carga_horaria;
        private string Turno;

        public Administrativo(string Nombre, string Apellido, int Edad, string Sexo, double Sueldo, int Años_servicio, string Cargo, int Carga_horaria, string Turno ) : base(Nombre, Apellido, Edad, Sexo, Sueldo, Años_servicio)
        {
            this.Cargo = "Administrador";
            this.Carga_horaria = Carga_horaria;
            this.Turno = Turno;
        }

        public string cargo
        {
            get
            {
                return Cargo;
            }
            set
            {
                Cargo = value;
            }
        }

        public int carga_horaria
        {
            get
            {
                return Carga_horaria;
            }
            set
            {
                Carga_horaria = value;
            }
        }

        public string turno
        {
            get
            {
                return Turno;
            }
            set
            {
                Turno = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}, Cargo: {1}, Carga horaria: {2}, Turno: {3}", base.ToString(), Cargo, Carga_horaria, Turno);
        }

    }
}
