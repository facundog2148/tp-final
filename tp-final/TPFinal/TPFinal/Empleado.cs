﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    class Empleado : Persona
    {
        private double Sueldo;
        private int Años_servicio;


        public Empleado(string Nombre, string Apellido, int Edad, string Sexo, double Sueldo, int Años_servicio) : base(Nombre, Apellido, Edad, Sexo)
        {
            this.Sueldo = Sueldo;
            this.Años_servicio = Años_servicio;
        }

        public double sueldo
        {
            get
            {
                return Sueldo;
            }
            set
            {
                Sueldo = value;
            }
        }
        public int años_servicio
        {
            get
            {
                return Años_servicio;
            }
            set
            {
                Años_servicio = value;
            }
        }

        public void AumentoSalario(double a)
        {
            Sueldo = Sueldo + a;
        }


        public override string ToString()
        {
            return String.Format("{0}, Sueldo: {1}, Antiguedad: {2}", base.ToString(), Sueldo, Años_servicio);
        }

    }
}
