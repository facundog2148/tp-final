﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    class Area
    {
        private string Nombre;
        private Empleado Jefe_Area;
        private List<Empleado> Empleados = new List<Empleado>();
        Auricular aur1 = new Auricular("Auricular", 1000,"Samsung",20,"inalámbrico",0,16, 10000);
        Bateria bat1 = new Bateria("Bateria", 5000, "Samsung", 50, "litio", 5000, 5);
        Cargador carg1 = new Cargador("Cargador", 8000, "Samsung", 25, "Portatil", 5);

        public Area(string Nombre)
        {
            this.Nombre = Nombre;
        }

        public string nombre
        {
            get
            {
                return Nombre;
            }
            set
            {
                Nombre = value;
            }
        }
        
        public void Jefe()
        {
            Random r = new Random();
            int i = r.Next(0, Empleados.Count - 1);

            Jefe_Area = Empleados[i];
        }

        public void agregarEmpleado(Empleado a)
        {
            Empleados.Add(a);
        }

        public override string ToString()
        {
            return String.Format("Nombre: {0}, Jefe de area: {1}", Nombre, Jefe_Area);
        }
    }
}
