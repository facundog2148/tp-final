﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    class Auricular : Producto
    {
        private string Tipo_auricular;
        private double Sensibilidad;
        private double Impedancia;
        private double Frecuencia_resp;

        public Auricular(string Nombre, int Precio, string Marca, int Cantidad, string Tipo_auricular, double Sensibilidad, double Impedancia, double Frecuencia_resp) : base(Nombre, Precio, Marca, Cantidad)
        {
            this.Tipo_auricular = Tipo_auricular;
            this.Sensibilidad = Sensibilidad;
            this.Impedancia = Impedancia;
            this.Frecuencia_resp = Frecuencia_resp;
        }

        public string tipo_cargador
        {
            get
            {
                return Tipo_auricular;
            }
            set
            {
                Tipo_auricular = value;
            }
        }
        public double sensibilidad
        {
            get
            {
                return Sensibilidad;
            }
            set
            {
                Sensibilidad = value;
            }
        }

        public double impedancia
        {
            get
            {
                return Impedancia;
            }
            set
            {
                Impedancia = value;
            }
        }

        public double frecuencia_resp
        {
            get
            {
                return Frecuencia_resp;
            }
            set
            {
                Frecuencia_resp = value;
            }
        }

        public int Stock
        {
            get
            {
                return base.Cantidad;

            }
            set
            {
                this.Cantidad = value;
            }
        }


        public override string ToString()
        {
            return String.Format("{0}, Tipo de auricular: {1}, Sensibilidad (dB): {2}, Impedancia (ohm): {3}, Frecuencia de respuesta: {4}", base.ToString(), Tipo_auricular, Sensibilidad, Impedancia, Frecuencia_resp);
        }

    }
}
