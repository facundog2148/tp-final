﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    public abstract class Producto
    {
        protected string Nombre;
        protected int Precio;
        protected string Marca;
        protected int Cantidad;

        public Producto(string Nombre, int Precio, string Marca, int Cantidad)
        {
            this.Nombre = Nombre;
            this.Precio = Precio;
            this.Marca = Marca;
            this.Cantidad = Cantidad;
        }

        public override string ToString()
        {
            return String.Format("Nombre del producto: {0}, Precio del producto: {1}, Marca del producto: {2}, Cantidad de producto: {3}", Nombre, Precio, Marca, Cantidad);
        }

    }
}
