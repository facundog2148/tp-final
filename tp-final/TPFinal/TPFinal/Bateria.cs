﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    class Bateria : Producto
    {
        private string Tipo_bateria;
        private double Capacidad;
        private double Potencia;

        public Bateria(string Nombre, int Precio, string Marca, int Cantidad, string Tipo_bateria, double Capacidad, double Potencia) : base(Nombre, Precio, Marca, Cantidad)
        {
            this.Tipo_bateria = Tipo_bateria;
            this.Capacidad = Capacidad;
            this.Potencia = Potencia;
        }

        public string tipo_bateria
        {
            get
            {
                return Tipo_bateria;
            }
            set
            {
                Tipo_bateria = value;
            }
        }
        public double capacidad
        {
            get
            {
                return Capacidad;
            }
            set
            {
                Capacidad = value;
            }
        }

        public double potencia
        {
            get
            {
                return Potencia;
            }
            set
            {
                Potencia = value;
            }
        }

        public int Stock
        {
            get
            {
                return base.Cantidad;

            }
            set
            {
                this.Cantidad = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}, Tipo de bateria: {1}, Capacidad (mAh): {2}, Potencia (A): {3}", base.ToString(), Tipo_bateria, Capacidad, Potencia);
        }

    }
}
