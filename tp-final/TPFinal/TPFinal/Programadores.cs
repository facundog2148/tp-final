﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPFinal
{
    class Programadores : Empleado
    {
        private string Cargo;
        private string Tipo_prog;
        private string Lenguaje;
        private int Carga_horaria;

        public Programadores(string Nombre, string Apellido, int Edad,string Sexo, double Sueldo, int Años_servicio, string Cargo, string Tipo_prog, string Lenguaje, int Carga_horaria) : base(Nombre, Apellido, Edad, Sexo, Sueldo, Años_servicio)
        {
            this.Cargo = "Programador";
            this.Tipo_prog = Tipo_prog;
            this.Lenguaje = Lenguaje;
            this.Carga_horaria = Carga_horaria;
        }

        public string cargo
        {
            get
            {
                return Cargo;
            }
            set
            {
                Cargo = value;
            }
        }

        public string tipo_prog
        {
            get
            {
                return Tipo_prog;
            }
            set
            {
                Tipo_prog = value;
            }
        }

        public string lenguaje
        {
            get
            {
                return Lenguaje;
            }
            set
            {
                Lenguaje = value;
            }
        }

        public int carga_horaria
        {
            get
            {
                return Carga_horaria;
            }
            set
            {
                Carga_horaria = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}, Tipo de programador: {1}, Lenguaje: {2}, Carga horaria: {3}", base.ToString(), Tipo_prog, Lenguaje, Carga_horaria);
        }

    }
}
